import {ModuleWithProviders, NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService, PAGE_TITLE_CONFIG, PageTitleInterface} from './services';
import {NicoComponentsModule} from './components';

@NgModule({
  imports: [HttpClientModule],
  providers: [ NicoPageTitle, NicoSessionService, NicoStorageService],
  exports: [NicoComponentsModule]
})
export class SystemModule {

  static forRoot(titleConf: PageTitleInterface): ModuleWithProviders<any> {
    return {
      ngModule: SystemModule,
      providers: [
        {provide: PAGE_TITLE_CONFIG, useValue: titleConf},
        AjaxSpinnerService
      ]
    };
  }

}
