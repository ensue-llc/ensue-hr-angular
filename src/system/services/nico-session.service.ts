import {AuthenticatableInterface} from '../interfaces';
import {Injectable} from '@angular/core';
import {NicoStorageService} from './nicoStorage.service';

@Injectable()
export class NicoSessionService {

  /**
   * Authenticated user
   * @protected
   */
  protected user: AuthenticatableInterface;

  public USER_STORE_KEY = 'auth_user';

  public TOKEN_STORE_KEY = 'msal.idtoken';

  /**
   * Constructor
   * @param storage
   */
  public constructor(protected storage: NicoStorageService) {

  }
  private awakeUserFromStorage(): void {
    this.user = JSON.parse(this.storage.getItem(this.USER_STORE_KEY));
  }
  /**
   * Set user in the session
   * @param user
   */
  public setUser(user: AuthenticatableInterface): void {
    this.user = user;
    this.storage.setItem(this.USER_STORE_KEY, JSON.stringify(user));
  }

  /**
   * Get the session user
   */
  public getUser(): AuthenticatableInterface {
    if (!this.user) {
      this.awakeUserFromStorage();
    }
    return this.user;
  }

  /**
   * Remove the user from session
   */
  public clearAuth(): void {
    this.user = null;
    this.storage.clear();
  }

  public setAuthToken(value): void {
    this.storage.setItem(this.TOKEN_STORE_KEY, value);
  }

  public getAuthToken(): string {
    return this.storage.getItem(this.TOKEN_STORE_KEY);
  }

}
