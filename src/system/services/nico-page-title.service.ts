import {Title} from '@angular/platform-browser';
import {Inject, Injectable, InjectionToken} from '@angular/core';

@Injectable()
export class NicoPageTitle {

  /**
   * Constructor
   */
  public constructor(@Inject(PAGE_TITLE_CONFIG) protected pageTitleConfig: PageTitleInterface, protected title: Title ){

  }

  private createTitle(str: string): string {
    if (this.pageTitleConfig.titleAddOn !== '' && this.pageTitleConfig.addOnShown === true) {
      if (this.pageTitleConfig.titlePrefixed === true) {
        str = `${this.pageTitleConfig.titleAddOn} ${this.pageTitleConfig.separator} ${str}`;
      } else {
        str = ` ${str} ${this.pageTitleConfig.separator} ${this.pageTitleConfig.titleAddOn} `;
      }
    }
    return str;
  }

  /**
   * Set the title of the document
   */
  public setTitle(str: string): void {
    this.title.setTitle(this.createTitle(str));
  }

  /**
   * Get current page title
   */
  public getTitle(): string {
    return this.title.getTitle();
  }


}

export const PAGE_TITLE_CONFIG = new InjectionToken('pageTitleConfig');

export interface PageTitleInterface {
  titleAddOn: string;

  titlePrefixed: boolean;

  separator: string;

  addOnShown: boolean;

}
