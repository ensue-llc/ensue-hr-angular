import {NicoPageTitle} from './nico-page-title.service';
import {Title} from '@angular/platform-browser';

describe('Nico Page Title Service', () => {
  const pageTitleConfig = {
    titlePrefixed: true,
    titleAddOn: '-',
    separator: ':',
    addOnShown: true
  };
  const title = new Title(document);

  it('should create page title service', () => {
    expect(new NicoPageTitle(pageTitleConfig, title)).toBeTruthy();
  });
});
