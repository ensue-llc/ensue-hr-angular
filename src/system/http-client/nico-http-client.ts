import {Inject, Injectable, InjectionToken, Optional} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {HttpRequestOptionsInterface} from './http-request-options-interface';
import {HttpMethod} from './http-method';
import {catchError, map} from 'rxjs/operators';
import {NicoCollection} from '../utilities';
import {isArrayLike} from 'rxjs/internal-compatibility';

@Injectable()
export class NicoHttpClient {

  protected defaultRequestOptions: HttpRequestOptionsInterface;

  /**
   * Constructor
   * @param http The base http client
   * @param tokenProvider Token Provider
   * @param commonRequestOptions Default request option
   * @param errorInterceptor The error interface
   */
  public constructor(private http: HttpClient,
                     @Optional() @Inject(NICO_AUTH_TOKEN_PROVIDER) protected tokenProvider?: NicoAuthorizationProviderInterface,
                     @Optional() @Inject(NICO_HTTP_REQUEST_OPTIONS) protected commonRequestOptions?: NicoDefaultRequestOptionsInterface,
                     @Optional() @Inject(NICO_HTTP_ERROR_INTERCEPTOR_PROVIDER) protected errorInterceptor?: NicoHttpErrorInterceptorInterface,
                     ) { }

 protected processDefaultOptions(options?: HttpRequestOptionsInterface): void {
    this.defaultRequestOptions = {
      responseType: 'json',
      headers: new HttpHeaders(),
      params: new HttpParams()
    };
    if (this.commonRequestOptions) {
      const params = this.commonRequestOptions.getDefaultParams();
      if (params) {
        this.defaultRequestOptions.params = params;
      }
      const headers =  this.commonRequestOptions.getDefaultHeaders();
      if (headers) {
        this.defaultRequestOptions.headers = headers;
      }
    }
    if (options && options.params) {
      const keys = options.params.keys();
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < keys.length; i ++) {
        this.defaultRequestOptions.params = this.defaultRequestOptions.params.set(keys[i], options.params.get(keys[i]));
      }
    }

 }

  /**
   * Add auth token to header
   * @private
   */
  private addAuthTokenToHeader(): HttpRequestOptionsInterface {
    if (this.tokenProvider && this.tokenProvider.useAuthTokenForRequest()) {
      const token = this.tokenProvider.getAuthToken();
      // @ts-ignore
      this.defaultRequestOptions.headers = this.defaultRequestOptions.headers.set('Authorization', 'Bearer ' + token);
    }
    return this.defaultRequestOptions;
  }

  private request(method: HttpMethod, url: string, body?: any,  options?: HttpRequestOptionsInterface): Observable<any> {
    this.processDefaultOptions(options);
    options = this.addAuthTokenToHeader();
    (options as any).body = body;
    return this.http.request(method as string, url, options )
      .pipe(map((response: Array<any>) => {
        if (isArrayLike(response)){
          return new  NicoCollection(response as any);
        }
        return response;
        }
      ), catchError((err: HttpErrorResponse) => {
        if (this.errorInterceptor) {
          this.errorInterceptor.onError(err);
        }
        return throwError(err);
      }));
  }

  public get(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.GET, url, null,  options);
  }

  public post(url: string, body?: any, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.POST, url, body, options);
  }

  public put(url: string, body?: any, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.PUT, url, body, options);
  }

  public patch(url: string, body?: any, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.PATCH, url, body, options);
  }

  public delete(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.DELETE, url, null, options);
  }

  public head(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.HEAD, url, null, options);
  }

  public purge(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.PURGE, url, null, options);
  }

  public copy(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.COPY, url, null, options);
  }

  public view(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.VIEW, url, null, options);
  }

  public link(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.LINK, url, null, options);
  }

  public unlink(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.UNLINK, url, null, options);
  }

  public propfind(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.POST, url, null, options);
  }

  public options(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.OPTIONS, url, null, options);
  }

  public lock(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.LOCK, url, options);
  }

  public unlock(url: string, options?: HttpRequestOptionsInterface): Observable<any> {
    return this.request(HttpMethod.UNLOCK, url, options);
  }
}

export const NICO_AUTH_TOKEN_PROVIDER = new InjectionToken('nicoAuthInjectionToken');

export interface NicoAuthorizationProviderInterface {
  useAuthTokenForRequest(): boolean;
  getAuthToken(): string;
}

export const NICO_HTTP_REQUEST_OPTIONS = new InjectionToken('nicoHttpRequestOptions');

export interface NicoDefaultRequestOptionsInterface {
  getDefaultHeaders(): HttpHeaders;
  getDefaultParams(): HttpParams;
}

export const NICO_HTTP_ERROR_INTERCEPTOR_PROVIDER = new InjectionToken('nicoErrorInterceptorProvider');

export interface NicoHttpErrorInterceptorInterface  {
  onError(error: HttpErrorResponse): void;
}
