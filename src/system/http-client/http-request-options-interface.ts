import {HttpHeaders, HttpParams} from '@angular/common/http';

export interface HttpRequestOptionsInterface {

  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  observe?: 'body';
  params?: HttpParams;
  reportProgress?: boolean;
  responseType?: 'arraybuffer'|'json';
  withCredentials?: boolean;
}
