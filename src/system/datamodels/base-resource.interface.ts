import {NicoCollection} from '../utilities';

export interface BaseResourceInterface {

  htmlId?: string;
  /**
   * The primary key identifier
   *  string
   */
  primaryKey: string;
  /**
   * The creatable Attributes
   *  Array<string>
   */
  creatableAttributes: Array<string>;
  /**
   * title of resource
   */
  title?: string;
  /**
   * Description of resource
   */
  description?: string;


  /**
   * Create
   */
  create(obj: any): BaseResourceInterface;

  /**
   * Create a collection
   */
  createFromCollection(items: NicoCollection<any>): NicoCollection<BaseResourceInterface>;

}
