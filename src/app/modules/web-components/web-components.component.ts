import {AfterViewInit, Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NotificationRowComponent} from '@common/components/notification-row/notification-row.component';
import {EmptyStateComponent} from '@common/components/empty-state/empty-state.component';
import {filter} from 'rxjs/operators';
import {Scroll} from '@angular/router';
import {ViewportScroller} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {AdvancedSelectConfigInterface, FlyMenuItemInterface} from '@system/components';
import {AbstractBaseComponent} from '../../AbstractBaseComponent';

@Component({
  selector: 'app-web-components',
  templateUrl: './web-components.component.html',
  styleUrls: ['./web-components.component.scss']
})
export class WebComponentsComponent extends AbstractBaseComponent implements OnInit, AfterViewInit {

  @ViewChild('dropDownItemTemplate') dropDownItemTemplate: TemplateRef<any>;
  public selectedElementId: string;

  public isDrawerOpen = false;

  public options = [
    {name: 'A very long name here'},
    {name: 'Name'},
    {name: 'Test Name'}
  ];

  public config: AdvancedSelectConfigInterface = {};

  public flyMenuAddOns: FlyMenuItemInterface[] = [
    {name: 3, label: 'Edit', active: true, faIcon: 'far fa-pen'},
    {name: 3, label: 'Delete', active: true, faIcon: 'far fa-trash'},
    {name: 3, label: 'Settings', active: true, faIcon: 'far fa-cog'}
  ];

  // Icons
  public iconList: any[] = [];

  // Switch Component
  public switchState: boolean;

  public componentsList = [
    {name: 'Typography', fragment: 'typography'},
    {name: 'Buttons', fragment: 'buttons'},
    {name: 'Grid', fragment: 'grid'},
    {name: 'Input', fragment: 'input'},
    {name: 'Form', fragment: 'form'},
    {name: 'Dropdown', fragment: 'dropdown'},
    {name: 'Advance Select', fragment: 'advSelect'},
    {name: 'Switch', fragment: 'switch'},
    {name: 'Tab', fragment: 'tab'},
    {name: 'Badge & Pills', fragment: 'badge'},
    {name: 'Circular Icons', fragment: 'circleIcons'},
    {name: 'Avatar', fragment: 'avatar'},
    {name: 'Notification', fragment: 'notification'},
    {name: 'Empty State', fragment: 'emptyState'},
    {name: 'Modal', fragment: 'modal'},
    {name: 'Skeleton', fragment: 'skeleton'},
  ];

  constructor(inject: Injector, private viewPortScroller: ViewportScroller, private dialog: MatDialog) {
    super(inject);
  }

  ngAfterViewInit(): void {
    this.config = {
      itemTemplate: this.dropDownItemTemplate,
      selectInfoLabel: 'Select One',
    };
  }

  ngOnInit(): void {
    this.pageTitleService.setTitle('My set title');

    // Tab active
    this.router.events.pipe(filter((e): e is Scroll => e instanceof Scroll)).subscribe(e => {
      if (e.anchor) {
        this.selectedElementId = e.anchor;
      } else {
        this.selectedElementId = this.componentsList[0].fragment;
      }
    });
  }

  public onSwitchValueChange($event): void {
    this.switchState = $event;
  }

  public openNotificationDialog(): void {
    this.dialog.open(NotificationRowComponent, {maxWidth: 720, maxHeight: 800});
  }

  public openEmptyComponentDialog(): void {
    const dialogRef = this.dialog.open(EmptyStateComponent, {});
  }

  public openSnackBar(): void {
    this.snackbar.open('Snackbar Message', '', {
      duration: 2000,
      verticalPosition: 'top',
      horizontalPosition: 'end'
    });
  }

  public onDrawerOpenCloseEvent(value): void {
    this.isDrawerOpen = value.isOpen;
  }

  public openDrawer($event): void {
    $event.stopPropagation();
    this.isDrawerOpen = true;
  }
}
