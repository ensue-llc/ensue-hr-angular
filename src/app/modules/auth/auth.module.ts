import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginComponent} from './pages/login/login.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import {HrCommonModule} from '@common/hr-common.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LogoutComponent } from './pages/logout/logout.component';
import {AuthRoutesModule} from './auth-routes.module';
import { CallbackComponent } from './pages/callback/callback.component';

@NgModule({
  declarations: [
    LoginComponent,
    ForgotPasswordComponent,
    LogoutComponent,
    CallbackComponent,
  ],
  imports: [
    HrCommonModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AuthRoutesModule,
  ],
  providers: [],
})
export class AuthModule { }
