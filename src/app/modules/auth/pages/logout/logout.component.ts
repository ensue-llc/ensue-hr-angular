import {Component, Injector, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
// import {AuthService} from '@common/services/auth.service';

@Component({
  selector: 'app-logout',
  template: '',
})
export class LogoutComponent extends AbstractBaseComponent implements OnInit {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.session.clearAuth();
    this.router.navigate(['login']);
  }
}
