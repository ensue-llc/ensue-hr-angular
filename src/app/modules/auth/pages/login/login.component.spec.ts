import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {NicoHttpClient} from '@system/http-client';
import {MockMatSnackBar, MockNicoHttpClient} from '@common/testing-resources/mock-services';
import {
  AjaxSpinnerService,
  NicoPageTitle,
  NicoSessionService,
  NicoStorageService
} from '@system/services';
import {MockNicoStorageService} from '@common/testing-resources/mock-services/MockNicoStorageService';
import {MockNicoPageTitle} from '@common/testing-resources/mock-services/MockNicoPageTitle';
import {MockNicoSessionService} from '@common/testing-resources/mock-services/MockNicoSessionService';
import {MockAjaxSpinnerService} from '@common/testing-resources/mock-services/MockAjaxSpinnerService';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {MockFormBuilder} from '@common/testing-resources/mock-services/MockFormBuilder';

fdescribe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      providers: [
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormBuilder, useClass: MockFormBuilder},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: AjaxSpinnerService, useClass: MockAjaxSpinnerService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useValue: {}},
        {provide: Router, useValue: {}},
        {provide: ActivatedRoute, useValue: {}},

      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create login component', () => {
    expect(component).toBeTruthy();
  });

  it('should enable user to log in', () => {
    component.createForm();
    spyOn(localStorage, 'setItem');
    component.formGroup.setValue({username: 'pallavi.ghimire@ensue.us', password: 'pallavi@hr'});
    component.logIn();
    expect(localStorage.setItem).toHaveBeenCalled();
  });

  // it('should redirect user to forgot password page', () => {
  //   fixture.detectChanges();
  //   component.forgotPassAction();
  //   expect(window.location.href).toEqual('http://localhost:4200/forgot-password');
  // });

  // it('should let user login via google or microsoft', () => {
  //   component.providerLogin('google');
  //   expect(component.loginPageRedirect).not.toBeNull();
  // });
});
