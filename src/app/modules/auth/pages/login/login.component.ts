import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AjaxSpinnerPositionEnum, NicoSessionService, NicoStorageService} from '@system/services';
import {UserModel} from '@common/models';
import {LoginService} from '@common/services/login.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * FormGroup
   */
  public formGroup: FormGroup;

  /**
   * Provider Login
   */

  public providerLink = 'http://localhost:8000/api/auth/login/';

  /**
   * Subscriptions
   */
  public loginSubscription: Subscription;

  public providerLoginSubscription: Subscription;

  /**
   * Constructor
   * @param injector
   * @param formBuilder
   * @param loginService
   * @param nicoStorageService
   */
  public constructor(
    injector: Injector, private formBuilder: FormBuilder,
    protected loginService: LoginService,
    protected nicoSessionService: NicoSessionService,
                     ) {
    super(injector);
  }

  /**
   * Lifecycle Method
   */
  ngOnInit(): void {
    this.createForm();
  }

  /**
   * To create the login form
   */
  public createForm(): void {
    this.formGroup = this.formBuilder.group({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  /**
   * To log user in and set the localstorage with user data
   */
  public logIn(): void {
    const spinner = this.spinnerService.showSpinner('', null, AjaxSpinnerPositionEnum.Fixed);
    this.loginSubscription = this.loginService.post(this.formGroup.getRawValue()).subscribe((userInfo: UserModel) => {
      spinner.hide();
      this.nicoSessionService.setUser(userInfo);
      this.router.navigate(['/dashboard']);
    });
  }

  public forgotPassAction(): void {

  }

  public ngOnDestroy(): void {
    if (this.loginSubscription != null) {
      this.loginSubscription.unsubscribe();
    }
  }
}
