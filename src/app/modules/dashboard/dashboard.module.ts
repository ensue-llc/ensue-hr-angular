import { NgModule } from '@angular/core';
import {HrCommonModule} from '@common/hr-common.module';
import {DashboardComponent} from './dashboard.component';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    HrCommonModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
})

export class DashboardModule { }
