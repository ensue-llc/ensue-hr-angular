export class HrChart {
  public chartLabels: string[] ;
  public plotDataUnits: string[];
  public uniqueUnits: string[];
  public options: { multiYAxes: boolean, } = {multiYAxes: false};

  constructor(public plotData?: any[], public labels?: any[]) {
    this.chartLabels = [];
    this.plotDataUnits = [];
    this.uniqueUnits = [];
  }

  public pushDataUnit(unit: string): void {
    this.plotDataUnits.push(unit);
    if (this.uniqueUnits.indexOf(unit) === -1) {
      this.uniqueUnits.push(unit);
    }
  }

  public plotUnitsAreDifferent(): boolean {
    const set = new Set(this.plotDataUnits);
    return set.size > 1;
  }

}
