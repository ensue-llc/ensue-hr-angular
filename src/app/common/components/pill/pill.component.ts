import {Component, Input, OnInit} from '@angular/core';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-pill',
  template: `
    <div class="pill" [ngClass]="'pill-'+color +' '+ 'size-'+size">
      <span>{{label}}</span>
      <div class="remove-icon" *ngIf="removable">
        <div class="svg-icon-container">
          <i class="far fa-times"></i>
        </div>
      </div>
    </div>
  `,
  styles: [`:host {
    display: inline-block;
  }`]
})
export class PillComponent implements OnInit {
  /**
   * Label for pill
   */
  @Input() label: string;
  /**
   * Background color for pill
   */
  @Input() color: 'light' | 'success' | 'danger' | 'warning' | 'info' | string;
  /**
   * Determines the visibility for close option on pill
   */
  @Input() removable: boolean;
  /**
   * Size option for pill
   */
  @Input() size: 'size-default' | 'size-medium' | 'size-small';

  constructor() {
  }

  ngOnInit(): void {
    this.label = NicoUtils.isNullOrUndefined(this.label) ? '' : this.label;
    this.color = NicoUtils.isNullOrUndefined(this.color) ? 'light' : this.color;
    this.removable = NicoUtils.isNullOrUndefined(this.removable) ? false : this.removable;
    this.size = NicoUtils.isNullOrUndefined(this.size) ? 'size-default' : this.size;
  }
}
