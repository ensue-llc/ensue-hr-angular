import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PillComponent} from './pill.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

fdescribe('NotificationRowComponent', () => {
  let component: PillComponent;
  let fixture: ComponentFixture<PillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PillComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create notification row component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the value of label on init', () => {
    expect(component.label).toEqual('');
  });
});
