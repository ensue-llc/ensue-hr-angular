import {ComponentFixture, TestBed} from '@angular/core/testing';

import {NotificationRowComponent} from './notification-row.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('NotificationRowComponent', () => {
  let component: NotificationRowComponent;
  let fixture: ComponentFixture<NotificationRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotificationRowComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create notification row component', () => {
    expect(component).toBeTruthy();
  });

  it('should set value of type as success on init', () => {
    expect(component.type).toEqual('success');
  });
});
