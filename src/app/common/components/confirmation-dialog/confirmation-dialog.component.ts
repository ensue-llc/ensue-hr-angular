import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ConfirmationDialogInterface} from '@common/components/confirmation-dialog/confirmation-dialog.interface';

@Component({
  selector: 'app-confirm-delete',
  templateUrl: './confirmation-dialog.component.html',
})
export class ConfirmationDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ConfirmationDialogInterface,
  ) { }

  public ngOnInit(): void {
    this.data.title = this.data.title ? this.data.title : 'Confirm Deletion';
    this.data.message = this.data.message ? this.data.message : 'Are you sure you want to delete the selected item?';
  }

  public closeModal(apply: boolean): void {
    this.dialogRef.close(apply);
  }

}
