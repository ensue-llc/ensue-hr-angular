export interface ConfirmationDialogInterface {
  title?: string;
  message?: string;
}
