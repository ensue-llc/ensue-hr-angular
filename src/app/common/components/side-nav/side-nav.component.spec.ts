import {SideNavComponent} from './side-nav.component';
import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RouterTestingModule} from '@angular/router/testing';

describe('SideNavComponent', () => {
  let component: SideNavComponent;
  let fixture: ComponentFixture<SideNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ SideNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create sidenav component', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle sidenav', () => {
    const $event = new MouseEvent('click');
    const isOpen = component.isOpen;
    component.toggleSideNav($event);
    expect(component.isOpen).toEqual(!isOpen);
  });
});
