import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})

export class SideNavComponent implements OnInit {

  public isOpen = false;
  public routerEventSubscription: Subscription;
  @Output() sidenavCollapse: EventEmitter<boolean> = new EventEmitter<boolean>();

  public sideNavLinks = [
    {link: '/components', label: 'Components', icon: 'far fa-chip'},
  ];

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.routerEventSubscription = this.router.events.pipe(filter(e => e instanceof NavigationStart)).subscribe(() => {
      this.isOpen = false;
      this.sidenavCollapse.emit(this.isOpen);
    });
  }

  public toggleSideNav($event): void {
    $event.stopPropagation();
    this.isOpen = !this.isOpen;
    this.sidenavCollapse.emit(this.isOpen);
  }
}
