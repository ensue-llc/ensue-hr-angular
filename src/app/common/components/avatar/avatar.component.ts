import {Component, Input, OnInit} from '@angular/core';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-avatar',
  template: `
    <span class="avatar {{avatarClassName}}" [style]="avatarStyles">
      <img [src]="imageLink" alt="profile avatar" class="avatar-img">
    </span>
  `,
  styles: [`:host {
    display: inline-block;
  }`]
})

export class AvatarComponent implements OnInit {
  /**
   * Avatar image's source / link
   */
  @Input() imageLink?: string;
  /**
   * Custom additional classname for avatar (eg: 'my-avatar'})
   */
  @Input() avatarClassName?: string;
  /**
   * Custom styles for avatar (eg: {borderRadius: '0.75rem'})
   */
  @Input() avatarStyles?: any;
  /**
   * Fallback image for avatar if imageLink is null or undefined
   */
  public fallbackImage = 'https://images.unsplash.com/photo-1510227272981-87123e259b17?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&s=3759e09a5b9fbe53088b23c615b6312e';

  constructor() {
  }

  ngOnInit(): void {
    this.imageLink = NicoUtils.isNullOrUndefined(this.imageLink) ? this.fallbackImage : this.imageLink;
    this.avatarClassName = NicoUtils.isNullOrUndefined(this.avatarClassName) ? '' : this.avatarClassName;
    this.avatarStyles = NicoUtils.isNullOrUndefined(this.avatarStyles) ? {} : this.avatarStyles;
  }
}
