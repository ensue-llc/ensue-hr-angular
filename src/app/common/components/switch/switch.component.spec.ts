import {SwitchComponent} from './switch.component';
import {ComponentFixture, TestBed} from '@angular/core/testing';

describe('SwitchComponent', () => {
  let component: SwitchComponent;
  let fixture: ComponentFixture<SwitchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwitchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create switch component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize values', () => {
    component.initConfig();
    expect(component.isActive).toBeDefined();
  });

  // it('should set value of isActive on change', () => {
  //   const $event = new Event('change');
  //   component.onSwitchToggle($event);
  //   expect(component.isActive).toBeDefined();
  // });

  it('should return an id in generateID', () => {
    const result = component.generateID();
    expect(result).toBeDefined();
  });
});
