import {Component, EventEmitter, HostBinding, Input, OnChanges, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-search-nav',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit, OnChanges {

  @HostBinding('class.page-header-right') add = true;

  @Input() config: SearchNavConfigInterface = {
    enableSearchInput: true,
    enableStatus: true,
    enableToggle: true
  };

  @Input() viewToggleIcon: string;
  @Input() dropdownOptions: any[];
  @Input() dropdownValue: any;

  @Input() placeholderText: string;

  @Output() searchChange: EventEmitter<string>;
  @Output() viewToggleClick: EventEmitter<MouseEvent>;
  @Output() dropdownChange: EventEmitter<any>;

  @ViewChild('dropDownItemTemplate') dropdownItemTemplate: TemplateRef<any>;

  public searchText = '';

  constructor() {
    this.viewToggleClick = new EventEmitter<MouseEvent>();
    this.searchChange = new EventEmitter<string>();
    this.dropdownChange = new EventEmitter<any>();
  }

  public initConfig(): void {
    this.config.enableSearchInput = NicoUtils.isNullOrUndefined(this.config.enableSearchInput) ? true : this.config.enableSearchInput;
    this.config.enableToggle = NicoUtils.isNullOrUndefined(this.config.enableToggle) ? true : this.config.enableToggle;
    this.config.enableStatus = NicoUtils.isNullOrUndefined(this.config.enableStatus) ? true : this.config.enableStatus;
  }

  private initDropdownOptions(): void {
    if (!this.dropdownOptions) {
      this.dropdownOptions = ['Active', 'Inactive'];
    }
  }

  ngOnChanges(): void {
    this.initDropdownOptions();
  }

  public ngOnInit(): void {
    this.initDropdownOptions();
    this.initConfig();
  }

  public toggleCollapsedView(evt: MouseEvent): void {
    this.viewToggleClick.emit(evt);
  }

  public onSelectItemPick(evt: any): void {
    this.dropdownChange.emit(evt);
  }

  public onSearchValueChange(): void {
    this.searchChange.emit(this.searchText);
  }

  public onSearchTextClear(): void {
    this.searchText = '';
    this.onSearchValueChange();
  }
}

export interface SearchNavConfigInterface {
  enableSearchInput?: boolean;
  enableStatus?: boolean;
  enableToggle?: boolean;
}
