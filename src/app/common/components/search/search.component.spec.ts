import {SearchComponent} from './search.component';
import {ComponentFixture, TestBed} from '@angular/core/testing';

describe('SwitchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create search nav component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize values', () => {
    component.initConfig();
    expect(component.config.enableStatus).toBeDefined();
  });

  it('should toggle collapsed view', () => {
    spyOn(component.viewToggleClick, 'emit').and.callThrough();
    const $event = new MouseEvent('click');
    component.toggleCollapsedView($event);
    expect(component.viewToggleClick.emit).toHaveBeenCalled();
  });

  it('should emit search text', () => {
    spyOn(component.dropdownChange, 'emit').and.callThrough();
    const $event = new Event('keypress');
    component.onSelectItemPick($event);
    expect(component.dropdownChange.emit).toHaveBeenCalled();
  });

  it('should change value of search change', () => {
    spyOn(component.searchChange, 'emit').and.callThrough();
    component.onSearchValueChange();
    expect(component.searchChange.emit).toHaveBeenCalled();
  });

  it('should clear search text', () => {
    component.searchText = 'Test';
    component.onSearchTextClear();
    expect(component.searchText).toEqual('');
  });
});

