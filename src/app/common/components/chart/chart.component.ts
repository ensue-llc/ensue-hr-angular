import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {HrChart} from '@common/utilities';
// @ts-ignore
import {ECharts} from 'echarts/core';
// @ts-ignore
import {EChartsOption} from 'echarts';
import {AppService} from '@common/services';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html'
})
export class ChartComponent implements OnInit, OnChanges {
  /**
   * Single Data Input
   */
  @Input() isChart: HrChart;

  @Input() selectedReadingList;

  public chartColors: string[] = ['#08A8dC', '#F96950', '#21C0B8', '#767AFB', '#A346EC', '#F03FDE'];

  protected chart: ECharts;

  public isLightMode: boolean;

  public options: EChartsOption = {
    tooltip: {
      trigger: 'axis',
      className: 'chart-tooltip'
    },
    xAxis: {
      type: 'category',
      data: [],
    },
    dataZoom: [{
      type: 'inside',
      start: 0,
      end: 100
    }, {
      start: 0,
      end: 100
    }],
    yAxis: {
      type: 'value',
    },
    series: [{
      data: [],
      type: 'line',
      smooth: true,
      lineStyle: {
        width: 1
      },
      emphasis: {
        lineStyle: {
          width: 0
        }
      }
    }],
    textStyle: {
      fontFamily: 'Rubik',
      fontSize: '14'
    },
    grid: {
      left: 28,
      top: 32,
      right: 28,
    },
  };

  /**
   * To get readings
   */
  public getReadings(): void {
    if (!this.isChart) {
      return;
    }
    this.chart.clear();
    this.options.series = [];

    this.options.yAxis = [];
    // tslint:disable-next-line:forin
    for (let i = 0; i < this.isChart.uniqueUnits.length; i++) {
      this.options.yAxis.push({
        type: 'value',
        position: i === 0 ? 'left' : 'right',
        name: this.isChart.uniqueUnits[i],
        id: 'y-axis-' + this.isChart.uniqueUnits[i],
        axisLine: {
          show: true,
          lineStyle: {
            color: '#747474',
          }
        }

      });
    }
    for (let i = 0; i < this.isChart.plotData.length; i++) {

      this.options.series.push({
        data: this.isChart.plotData[i],
        type: 'line',
        smooth: true,
        name: this.isChart.chartLabels[i],
        yAxisId: 'y-axis-' + this.isChart.plotDataUnits[i],
        color: this.chartColors[i],
        lineStyle: {
          width: 2
        },
        emphasis: {
          lineStyle: {
            width: 2.5
          }
        }
      });
    }

    if ('data' in this.options.xAxis) {
      this.options.xAxis.data = this.isChart.labels;
    }

    this.chart.setOption(this.options);
  }


  constructor(private appService: AppService) {
    this.appService.lightModeSubject.subscribe((isLightMode) => {
      this.isLightMode = isLightMode;
    });
  }

  public ngOnInit(): void {

  }

  public ngOnChanges(): void {
    this.getReadings();
  }

  public onChartInit(chart: ECharts): void {
    this.chart = chart;
  }

}
