// import {HeaderComponent} from './header.component';
// import {ComponentFixture, TestBed} from '@angular/core/testing';
// import {NicoSessionService} from '../../../../system/services';
// // @ts-ignore
// import {MockNicoSessionService} from '../../testing-resources/mock-services';
// import {RouterModule} from '@angular/router';
// import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
//
// describe('Header Component', () => {
//   let component: HeaderComponent;
//   let fixture: ComponentFixture<HeaderComponent>;
//
//   beforeEach(async () => {
//     await TestBed.configureTestingModule({
//       declarations: [ HeaderComponent ],
//       imports: [RouterModule.forRoot([])],
//       providers: [
//         {provide: NicoSessionService, useClass: MockNicoSessionService}
//       ],
//       schemas: [CUSTOM_ELEMENTS_SCHEMA],
//     })
//       .compileComponents();
//   });
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(HeaderComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create header component', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('should change theme on toggle', () => {
//     component.lightTheme = false;
//     component.onThemeToggle();
//     expect(component.lightTheme).toBeTrue();
//   });
//
//   it('should emit notification value on calling openNotificationDrawer function', () => {
//     spyOn(component.drawerOpen, 'emit');
//     const $event = new MouseEvent('click');
//     component.openNotificationDrawer($event);
//     expect(component.drawerOpen.emit).toHaveBeenCalled();
//   });
//
//   it('should return value when getAddOnMenuItems is triggered', () => {
//     const values = component.getAddOnMenuItems();
//     expect(values).toBeTruthy();
//   });
//
//   // it('should navigate you to web-components upon user_profile as an input to onFlyMenuAction', () => {
//   //   spyOn(component.router, 'navigate');
//   //   component.onFlyMenuAction('user_profile');
//   //   expect(component.router.navigate).toHaveBeenCalled();
//   // });
// });
