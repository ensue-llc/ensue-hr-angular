import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DrawerContainerComponent} from './drawer-container.component';

describe('DrawerContainerComponent', () => {
  let component: DrawerContainerComponent;
  let fixture: ComponentFixture<DrawerContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrawerContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawerContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create drawer container component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the value of position as "start" on init', () => {
    expect(component.position).toEqual('start');
  });

  it('should emit closeDrawer value on closing drawer', () => {
    spyOn(component.closeDrawerEvent, 'emit');
    const $event = new MouseEvent('click');
    component.closeDrawer($event);
    expect(component.closeDrawerEvent.emit).toHaveBeenCalled();
  });
});
