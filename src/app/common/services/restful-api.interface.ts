import {Observable} from 'rxjs';

export interface RestfulApiInterface {

  getResourceName(): string;

  getResourceUrl(): string;

  get(params?: any): Observable<any>;

}
