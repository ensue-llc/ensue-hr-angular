import {EventEmitter, Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {NicoStorageService} from '@system/services';

@Injectable({providedIn: 'root'})
export class AppService {

  protected STORE_KEY_LIGHT_MODE = 'is.app.theme.lightMode';

  protected statusChangeEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * TODO: isLightTheme should be stored on localstorage
   */
  public lightModeSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(protected storage: NicoStorageService) {
  }

  /**
   * Enable light mode
   * lightMode
   */
  public enableLightMode(lightMode: boolean): void {

    this.storage.setItem(this.STORE_KEY_LIGHT_MODE, lightMode ? '1' : '0');
    this.lightModeSubject.next(lightMode);

    const rootHTML = document.querySelector('html');
    if (lightMode) {
      rootHTML.className = 'theme-light';
    } else {
      rootHTML.className = 'theme-dark';
    }

  }

  public isLightMode(): boolean {
    return this.storage.getItem(this.STORE_KEY_LIGHT_MODE) !== '0';
  }

  public isDarkMode(): boolean {
    return !this.isLightMode();
  }

  public setThemeModeFromStorage(): void {
    const val = this.storage.getItem(this.STORE_KEY_LIGHT_MODE);
    this.enableLightMode(val !== '0');
  }

  public setScoutStatusChangeEmitter(status: boolean): void {
    this.statusChangeEmitter.emit(status);
  }

  public getScoutStatusChangeEmitter(): EventEmitter<boolean> {
    return this.statusChangeEmitter;
  }
}
