import { Injectable } from '@angular/core';
import {AbstractBaseService} from '@common/services/abstract-base.service';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService extends AbstractBaseService{

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
  }

  protected baseModel: BaseResource;

  getResourceName(): string {
    return 'auth/login';
  }

  // tslint:disable-next-line:typedef
  public get(params?: HttpParams, url?: string) {
    return super.get(params, this.getResourceUrl() + '/' + url);
  }
}
