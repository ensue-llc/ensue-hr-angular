import { Injectable } from '@angular/core';
import {AbstractBaseService} from '@common/services/abstract-base.service';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends AbstractBaseService{

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
  }

  protected baseModel: BaseResource;

  getResourceName(): string {
    return 'departments';
  }
}
