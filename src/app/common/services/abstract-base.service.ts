import {Injectable} from '@angular/core';
import {NicoHttpClient} from '@system/http-client';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {NicoCollection, NicoUtils} from '@system/utilities';
import {BaseResource, BaseResourceInterface} from '@system/datamodels';
import {environment} from '../../../environments/environment';
import {RestfulApiInterface} from './restful-api.interface';
import {HttpParams} from '@angular/common/http';
import {isArray} from 'rxjs/internal-compatibility';

@Injectable()
export abstract class AbstractBaseService  implements RestfulApiInterface {

  protected abstract baseModel: BaseResource;

  protected resourceUrl = '';

  protected env: any;

  public abstract getResourceName(): string;

  protected constructor(protected httpClient: NicoHttpClient) {
    this.env = environment;
    this.generateResourceUrl();
  }

  protected generateResourceUrl(): void {
    this.resourceUrl = this.env.api + this.getResourceName();
  }

  public getResourceUrl(): string {
    return this.resourceUrl;
  }

  public get(params?: HttpParams, url?: string): Observable<any> {
    if (NicoUtils.isNullOrUndefined(url)){
      url = this.resourceUrl;
    }
    return this.httpClient.get(url, {params, }).pipe(map((response: any) => {

      if (response instanceof NicoCollection || response instanceof Array || isArray(response)) {
        return this.baseModel.createFromCollection(response as any) as NicoCollection<BaseResourceInterface>;
      } else {
        return this.baseModel.create(response) as BaseResourceInterface;
      }
    }));
  }

  public post(body, params?: HttpParams): Observable<any> {
    return this.httpClient.post(this.resourceUrl, body, {params, });
  }

  public delete(occasion: any, params?: HttpParams): Observable<any> {
    params = params || new HttpParams();
    params = params.set('id', occasion.id);
    return this.httpClient.delete(this.resourceUrl, {params, });
  }

}
