import {Router} from '@angular/router';
import {NicoSessionService} from '@system/services';
import {Injectable} from '@angular/core';
import {NicoHttpErrorInterceptorInterface} from '@system/http-client';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class HttpErrorInterceptor implements NicoHttpErrorInterceptorInterface {
  constructor(protected router: Router,  protected session: NicoSessionService) {
  }

  onError(error: HttpErrorResponse): void {
    if (error.status === 401) {
      this.router.navigate(['/login']).then();
    }
  }
}
