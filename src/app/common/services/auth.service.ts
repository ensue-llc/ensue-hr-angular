// // @ts-ignore
// import {Observable, Subject} from 'rxjs';
// // @ts-ignore
// import {environment} from '@environment/environment';
// import {Injectable} from '@angular/core';
// import {NicoSessionService} from '@system/services';
// // @ts-ignore
// import {UserProfileModel} from '@common/models/user-profile.model';
//
// @Injectable()
// export class AuthService {
//
//
//   protected userModel: UserProfileModel = new UserProfileModel();
//
//   constructor(protected session: NicoSessionService) {
//   }
//
//
//
//   public msalHandler(errorDesc: any, token: any, error: any, tokenType: any): void {
//     if (errorDesc.indexOf('AADB2C90118') > -1) {
//       // User forgot password
//       localStorage.setItem('changePassword', 'true');
//     } else if (errorDesc.indexOf('AADB2C90077') > -1) {
//       // Expired Token, function call from interceptor with proper context
//       this.logout();
//     }
//   }
//
//   public login(): Observable<any> {
//     return this.authenticate();
//   }
//
//   // public signUp(): Observable<any> {
//   //   this.clientApplication.authority = this.authority + this.tenantConfig.signUpPolicy;
//   //   return this.authenticate();
//   // }
//
//   // public forgotPassword(): void {
//   //   this.clientApplication.authority = this.authority + this.tenantConfig.passwordResetPolicy;
//   //   this.clientApplication.loginRedirect({scopes: this.tenantConfig.b2cScopes});
//   // }
//
//   public authenticate(): Observable<any> {
//     const sub: Subject<any> = new Subject<any>();
//     this.clientApplication.loginRedirect({scopes: this.tenantConfig.b2cScopes, prompt: 'login'});
//     this.clientApplication.acquireTokenSilent({scopes: this.tenantConfig.b2cScopes}).then(() => {
//       this.awakeAuthUserToSession();
//       sub.next();
//     }).catch(err => {
//       console.log(err);
//     });
//     return sub;
//   }
//
//   public logout(): Observable<any> {
//     const sub = new Subject();
//     setTimeout(() => {
//       this.clientApplication.logout();
//       this.session.clearAuth();
//       sub.next();
//     }, 100);
//     return sub;
//   }
//
//   public isLoggedIn(): boolean {
//     return this.clientApplication.getAccount() != null;
//   }
//
//   // // 'https://login.microsoftonline.com/tfp/'
//   // public refreshToken(): Promise<any> {
//   //   return this.clientApplication.acquireTokenSilent({scopes: this.tenantConfig.b2cScopes });
//   // }
//   //
//   // public getAuthenticationToken(): Promise<any> {
//   //   return this.clientApplication.acquireTokenSilent({scopes: this.tenantConfig.b2cScopes})
//   //     .then(token => token)
//   //     .catch(error => {
//   //       return Promise.reject(error);
//   //     });
//   // }
//
//   // public awakeAuthUserToSession(): void {
//   //   const acc: Account = this.clientApplication.getAccount();
//   //   if (acc != null) {
//   //     this.session.setUser(this.userModel.create(acc));
//   //   } else {
//   //     this.session.clearAuth();
//   //   }
//   // }
//
// }
