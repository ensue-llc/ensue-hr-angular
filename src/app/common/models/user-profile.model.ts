import {BaseResource} from '@system/datamodels';
import {AuthenticatableInterface} from '@system/interfaces';

export class UserModel extends BaseResource implements AuthenticatableInterface{
  public creatableAttributes = [
    'body',
    'status',
  ];

  getEmail(): string {
    return this.email;
  }

  getId(): string {
    return this.id;
  }
}
