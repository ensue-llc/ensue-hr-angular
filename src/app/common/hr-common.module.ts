import {NgModule} from '@angular/core';
import {AvatarComponent} from './components/avatar/avatar.component';
import {BadgeComponent} from './components/badge/badge.component';
import {CircularIconComponent} from './components/circular-icon/circular-icon.component';
import {HeaderComponent} from './components/header/header.component';
import {PillComponent} from './components/pill/pill.component';
import {SideNavComponent} from './components/side-nav/side-nav.component';
import {SwitchComponent} from './components/switch/switch.component';
import {AuthMiddleware, GuestMiddleware} from './middlewares';
import {
  HttpErrorInterceptor, LoginService,
} from './services';
import {MatIconModule} from '@angular/material/icon';
import {CommonModule} from '@angular/common';
import {NotificationRowComponent} from './components/notification-row/notification-row.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {NicoComponentsModule} from '@system/components';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EmptyStateComponent} from './components/empty-state/empty-state.component';
import {MatDialogModule} from '@angular/material/dialog';
import {DrawerContainerComponent} from './components/drawer-container/drawer-container.component';
import {NotificationDrawerComponent} from './components/notification-drawer/notification-drawer.component';
import {ConfirmationDialogComponent} from '@common/components/confirmation-dialog/confirmation-dialog.component';
import {SearchComponent} from '@common/components/search/search.component';
// import {LoginComponent} from '../modules/auth/pages/login/login.component';


/*** Items within declarations, exports, imports etc. should be in a separate line ****/

@NgModule({
  declarations: [
    AvatarComponent,
    BadgeComponent,
    CircularIconComponent,
    HeaderComponent,
    PillComponent,
    SideNavComponent,
    SwitchComponent,
    NotificationRowComponent,
    EmptyStateComponent,
    DrawerContainerComponent,
    NotificationDrawerComponent,
    ConfirmationDialogComponent,
    SearchComponent,
    // LoginComponent,

    /*** Pipes ***/
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatSnackBarModule,
    MatDialogModule,
    /*** put pages export above and components export below this line ****/

    AvatarComponent,
    BadgeComponent,
    CircularIconComponent,
    HeaderComponent,
    PillComponent,
    SideNavComponent,
    SwitchComponent,
    NotificationRowComponent,
    EmptyStateComponent,
    DrawerContainerComponent,
    NotificationDrawerComponent,
    ConfirmationDialogComponent,
    SearchComponent,
  ],
  providers: [
    AuthMiddleware,
    GuestMiddleware,
    HttpErrorInterceptor,
    LoginService,
  ],
  imports: [
    RouterModule,
    CommonModule,
    MatIconModule,
    MatSnackBarModule,
    NicoComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
  ],
})

export class HrCommonModule {

}
