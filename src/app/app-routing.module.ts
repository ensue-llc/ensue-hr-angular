import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WebComponentsComponent} from './modules/web-components/web-components.component';
import {LoginComponent} from './modules/auth/pages/login/login.component';
import {DashboardComponent} from './modules/dashboard/dashboard.component';
import {AuthModule} from './modules/auth/auth.module';

/**
 * App Routes
 */
const routes: Routes = [
  // {path: 'auth', loadChildren: () => AuthModule},
  // {path: '**', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'web-components', component: WebComponentsComponent},
  {path: 'dashboard', component: DashboardComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    anchorScrolling: 'enabled',
    scrollOffset: [0, 100],
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
