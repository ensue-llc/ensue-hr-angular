import {Component, Injector, OnInit} from '@angular/core';
import {AbstractBaseComponent} from './AbstractBaseComponent';
import {IconRegistryService} from '@common/services/icon-registry.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends AbstractBaseComponent implements OnInit {
  title = 'ensue-hr';

  public isSidenavOpen: boolean;
  public isDrawerOpen: boolean;

  constructor(injector: Injector, private iconRegistryService: IconRegistryService) {
    super(injector);
    this.iconRegistryService.registerIcons();
  }

  public ngOnInit(): void {
    this.appService.setThemeModeFromStorage();
  }

  /**
   * Sidenav Collapse Event
   */
  public onSidenavCollapse($event): void {
    this.isSidenavOpen = $event;
    if (this.isSidenavOpen) {
      document.body.setAttribute('style', 'overflow-x: hidden');
    } else {
      setTimeout(() => {
        document.body.removeAttribute('style');
      }, 300);
    }
  }

  /**
   * Drawer Event
   */
  public notificationDrawerOpen($event): void {
    this.isDrawerOpen = $event;
  }

  public onDrawerOpenCloseEvent(eventData): void {
    this.isDrawerOpen = eventData.isOpen;
  }

}
