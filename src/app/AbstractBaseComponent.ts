import {Injectable, Injector} from '@angular/core';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService} from '@system/services';
import {NicoHttpClient} from '@system/http-client';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Location} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {AppService} from '@common/services';
import {MatDialogRef} from '@angular/material/dialog/dialog-ref';
import {ConfirmationDialogInterface} from '@common/components/confirmation-dialog/confirmation-dialog.interface';
import {ConfirmationDialogComponent} from '@common/components/confirmation-dialog/confirmation-dialog.component';

@Injectable()

export abstract class AbstractBaseComponent {

  protected pageTitleService: NicoPageTitle;

  protected httpClient: NicoHttpClient;

  protected session: NicoSessionService;

  protected spinnerService: AjaxSpinnerService;

  protected snackbar: MatSnackBar;

  protected modalService: MatDialog;

  protected router: Router;

  protected activatedRoute: ActivatedRoute;

  protected location: Location;
  /**
   * The page title for the component. It will be displayed on the tab/windows of the browser
   */
  protected pageTitle: string;

  public appService: AppService;

  protected constructor(injector: Injector) {
    this.pageTitleService = injector.get(NicoPageTitle);
    this.httpClient = injector.get(NicoHttpClient);
    this.session = injector.get(NicoSessionService);
    this.spinnerService = injector.get(AjaxSpinnerService);
    this.snackbar = injector.get(MatSnackBar);
    this.modalService = injector.get(MatDialog);
    this.router = injector.get(Router);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.location = injector.get(Location);
    this.appService = injector.get(AppService);
  }

  // tslint:disable-next-line:use-lifecycle-interface contextual-lifecycle
  public ngOnInit(): void {
    this.pageTitleService.setTitle(this.pageTitle);
  }

  /**
   * Go one step back to the location
   */
  goBack(): void {
    this.location.back();
  }

  public log(data: any): void {
    console.log(data);
  }

  public snackBarMessage(message: string, timeout?: number): void {
    timeout = timeout ? timeout : 3000;
    this.snackbar.open(message, undefined, {verticalPosition: 'top', duration: timeout});
  }

  /**
   * Confirmation Dialog Box Trigger
   */
  public openConfirmationDialogBox(data?: ConfirmationDialogInterface): MatDialogRef<any> {
    return this.modalService.open(ConfirmationDialogComponent, {data});
  }

}
