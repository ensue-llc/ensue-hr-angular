import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {WebComponentsComponent} from './modules/web-components/web-components.component';
import {HrCommonModule} from '@common/hr-common.module';
import {SystemModule} from '@system/system.module';
import {PageTitleInterface} from '@system/services';
import {NicoHttpClient} from '@system/http-client';
import {AuthModule} from './modules/auth/auth.module';
// import { DashboardComponent } from './modules/dashboard/dashboard.component';

const PAGE_TITLE_CONF: PageTitleInterface = {
  addOnShown: true,
  separator: '-',
  titleAddOn: 'Ensue HR',
  titlePrefixed: true
};

@NgModule({
  declarations: [
    AppComponent,
    WebComponentsComponent,
    // DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HrCommonModule,
    AuthModule,
    SystemModule.forRoot(PAGE_TITLE_CONF),
  ],
  providers: [NicoHttpClient],
  bootstrap: [AppComponent],
})

export class AppModule {
}
